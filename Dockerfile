FROM node

RUN mkdir /flatris
WORKDIR /flatris
COPY package.json /flatris
RUN yarn install

COPY . /flatris
RUN yarn test
RUN yarn build

CMD yarn start
expose 3000